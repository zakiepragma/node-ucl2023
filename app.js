const express = require("express");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
const port = 3001;

// setup body parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// setup database connection
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "node_ucl2023",
});

connection.connect((err) => {
  if (err) {
    console.error("Error connecting to database: " + err.stack);
    return;
  }
  console.log("Connected to database.");
});

app.get("/", (req, res) => {
  res.send("API From Express JS!");
});

// Gunakan middleware cors()
app.use(cors());

// create endpoint to retrieve all clubs
app.get("/clubs", (req, res) => {
  const sql = "SELECT * FROM clubs";
  connection.query(sql, (error, results, fields) => {
    if (error) {
      console.error("Error retrieving clubs: " + error.stack);
      return;
    }
    res.send(results);
  });
});

// create endpoint to retrieve a specific club
app.get("/clubs/:id", (req, res) => {
  const id = req.params.id;
  const sql = "SELECT * FROM clubs WHERE id = ?";
  connection.query(sql, [id], (error, results, fields) => {
    if (error) {
      console.error("Error retrieving club: " + error.stack);
      return;
    }
    res.send(results[0]);
  });
});

// create endpoint to create a new club
app.post("/clubs", (req, res) => {
  const name = req.body.name;
  const sql = "INSERT INTO clubs (name) VALUES (?)";
  connection.query(sql, [name], (error, results, fields) => {
    if (error) {
      console.error("Error creating club: " + error.stack);
      return;
    }
    res.send("Club created successfully.");
  });
});

// create endpoint to update a club
app.put("/clubs/:id", (req, res) => {
  const id = req.params.id;
  const name = req.body.name;
  const sql = "UPDATE clubs SET name = ? WHERE id = ?";
  connection.query(sql, [name, id], (error, results, fields) => {
    if (error) {
      console.error("Error updating club: " + error.stack);
      return;
    }
    res.send("Club updated successfully.");
  });
});

// create endpoint to delete a club
app.delete("/clubs/:id", (req, res) => {
  const id = req.params.id;
  const sql = "DELETE FROM clubs WHERE id = ?";
  connection.query(sql, [id], (error, results, fields) => {
    if (error) {
      console.error("Error deleting club: " + error.stack);
      return;
    }
    res.send("Club deleted successfully.");
  });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
